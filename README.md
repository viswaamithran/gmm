#Project Title
A suit of programmes for 3D forward and inversion modelling of gravity and magnetic field data.

##Getting Started
Comming soon...

##Prerequisites
gfortran, openmp_lib, netcdf

##Give examples
Comming soon...

##Installing
Development version:
1. Run bootstrap.sh 
2. ./configure
3. make
4. make install
```
Give the example
```

##Deployment

Add additional notes about how to deploy this on a live system
##Built With

    netcdf - Fortran netcdf module
    openmp - Openmp library for parallelising otherwise mundane loops

##Contributing
Comming soon...

##Authors

    MN

See also the list of contributors who participated in this project.
##License

This project is licensed under GPL.

##Acknowledgments
Richard Blakeley, 2004


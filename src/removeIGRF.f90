! removeIGRF.f90  : removes IGRF values from magnetic field data
!                   reads a file or terminal for magnetic field data and
!                   removes IGRF value from the observed mag field values.
!                   Expects the input in this format:
!                   YYYY/MM/DD HH:MM:SS.ss dddd.dddddddddd ddd.dddddddddd ddddd.dd 
!                   (year, date, lon, lat, m_obs(nT))
!                   70 FORMAT (A10XA11XF15.10xF14.10XF8.2)
!                   Outputs in this format:
!                   (year, date, lon, lat, m_obs(nT), IGRF(nT), m_obs-IGRF(nT))
!                   71 FORMAT (A10XA11XF15.10xF14.10XF8.2XF8.2XF8.2)
!                   Uses igrf12syn from igrf_subrtn.f file (F77) which is 
!                   copied from igrf12.f written by Susan Macmillan of BGS, 
!                   as given in NOAA's site.
! Usage           : removeigrf  OR removeigrf -f<filename>
!
! LICENSE:
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
program removeIGRF

    implicit none
    integer, parameter :: sp = kind(1e0)
    integer, parameter :: dp = kind(1.0d0)

    real(dp), parameter :: pi = 3.1415927

    integer :: i, j, k
    real :: lon, lat, alt, m_obs
    real(dp) :: d_date, elon, colat, d_alt, fx, fy, fz, f_total
    real(dp) :: dfx, dfy, dfz, df_total
    integer :: ios, uno, year, month, day, nargs, jday, tdays
    character (len = 256) :: argsi
    character (len = 2  ) :: switchi
    character (len = 254) :: optioni
    character (len = 256) :: ifilename
    character (len = 10 ) :: date
    character (len = 11) :: time

    logical :: if_flag = .FALSE.

    nargs = command_argument_count()
    ios = 1
    uno = 5 

    if( nargs .gt. 0 ) then
        call get_command_argument( 1, argsi )

        switchi = argsi(1:2)
        optioni = argsi(3:)

        if( switchi == "-f" ) then
            ifilename = trim(optioni)
            if_flag = .TRUE.
        endif
    endif

    if( if_flag .eqv. .TRUE. ) then
        uno = 16
        open( uno, FILE=ifilename, STATUS='OLD', IOSTAT=ios )
    else
        ios = 0
    endif

    if( ios /= 0 ) then
        print *, "FATAL ERROR: ERROR OPENING FILE"
        stop
    endif

    !READ DATE, TIME, LON, LAT, MAG_VAL
    !LOOP OVER TILL THE END OF INPUT
    do
        read(uno, 70, IOSTAT=ios) date, time, lon, lat, m_obs
        if( ios .lt. 0 ) then
            exit
        endif

        read(date(1:4), *) year
        read(date(6:7), *) month
        read(date(9:10),*) day
        d_date = 1.0 * year
        d_alt = 0.0
        colat = 90.0 - lat
        elon  = 1.0 * lon

        call igrf12syn( 0, d_date, 1, d_alt, colat, elon, fx, fy, fz, f_total)
        call igrf12syn( 1, d_date, 1, d_alt, colat, elon, dfx, dfy, dfz, df_total)
        call julian_day( year, month, day, jday, tdays )

        !calculate fx, fy, fz with linear extrapolation using annual variation
        !values and julian day of the target date
        dfx = dfx * jday *(1.0/tdays)
        dfy = dfy * jday *(1.0/tdays)
        dfz = dfz * jday *(1.0/tdays)

        f_total = sqrt( (fx+dfx)**2 + (fy+dfy)**2 + (fz+dfz)**2 )

        write(*,71) date, time, lon, lat, m_obs, f_total, m_obs-f_total
    enddo

    close(uno)

    70 FORMAT (A10,1X,A11,1X,F15.10,1x,F14.10,1X,F8.2)
    71 FORMAT (A10,1X,A11,1X,F15.10,1x,F14.10,1X,F8.2,1X,F8.2,1X,F8.2)


end program removeIGRF

subroutine julian_day( iyear, imonth, imday, iyday, tdays )
    !return julian day of a given date and also the total number of 
    !days in the given year.
    !Input iyear, imonth, imday as integers
    !Output iyday, tdays as integers
    implicit none
    integer, intent(in) :: iyear, imonth, imday
    integer, intent(out):: iyday, tdays
    integer :: i

    integer, dimension(12) :: d_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if( mod(iyear,4)==0 .and. mod(iyear,100)/=0 ) then
        d_months(2) = 29
        tdays = 366
    else
        tdays = 365
    end if

    iyday = 0
    do i=1, imonth - 1
        iyday = iyday + d_months(i)
    end do
    iyday = iyday + imday

end subroutine julian_day




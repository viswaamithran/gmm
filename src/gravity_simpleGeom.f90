! gravity_simpleGeom.f90: contains subroutines/functions to
!                         calculate gravity field due to simple
!                         geometrical bodies with known density
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
module gravity_simpleGeom

    use dataFormats
    implicit none

    real(dp) :: isign(2), gamma, si2mg, km2m
    data isign/1,-1/, gamma/6.67408e-11/, si2mg/1.e5/, km2m/1.e3/

contains

    subroutine v_dyke_2D(x0, x_1, x_2, z1, z2, rho, g)   
        real, intent(in) :: x0, x_1, x_2, z1, z2, rho
        real :: x1, x2
        real, intent(out):: g
        real :: theta1, theta2, theta3, theta4
        real :: r1, r2, r3, r4

        x1 = x_1 - x0
        x2 = x_2 - x0

        r1 = sqrt( x1**2 + z1**2 )
        r2 = sqrt( x1**2 + z2**2 )
        r3 = sqrt( x2**2 + z1**2 )
        r4 = sqrt( x2**2 + z2**2 )

        theta1 = atan2( x1, z1 )
        theta2 = atan2( x1, z2 )
        theta3 = atan2( x2, z1 )
        theta4 = atan2( x2, z2 )

        g = z2 * ( theta2 - theta4 ) - z1 * ( theta1 - theta3 )
        g = g + ( x2 - x1 ) * log( r4/r3 )

        g = 2 * gamma * rho * g

    endsubroutine

    subroutine v_rect_prism(x0, y0, z0, x1, y1, z1, x2, y2, z2, rho, g)
        real, intent(in) :: x0, y0, z0, x1, y1, z1, x2, y2, z2, rho
        real(dp), intent(out) :: g
        real(dp) :: x(2), y(2), z(2), summ
        real(dp) :: rijk, arg1, arg2, arg3
        integer :: muijk, i, j, k

        g = si2mg

        x(1) = (x1 - x0)
        x(2) = (x2 - x0)
        y(1) = (y1 - y0)
        y(2) = (y2 - y0)
        z(1) = (z1 - z0)
        z(2) = (z2 - z0)

        summ = 0.0

        do i = 1, 2
            do j = 1, 2
                do k = 1, 2
                    rijk = sqrt( x(i)**2 + y(j)**2 + z(k)**2 )
                    muijk  = isign(i) * isign(j) * isign(k)

                    arg1 = atan2( (x(i) * y(j)), (z(k)*rijk) )
                    !                if (arg1 .lt. 0.) arg1 = arg1 + twopi

                    if( x(i) == 0 ) then
                        arg2 = 1.0
                    else
                        arg2 = ( rijk + y(j) ) / ( rijk - y(j) )
                    endif

                    if(y(j) == 0 ) then
                        arg3 = 1.0
                    else
                        arg3 = ( rijk + x(i) ) / ( rijk - x(i) )
                    endif

                    arg2 = log(arg2)
                    arg3 = log(arg3)

                    summ = summ + muijk*( z(k)*arg1 - 0.5*x(i)*arg2 - 0.5*y(j)*arg3 )

                enddo
            enddo
        enddo

        g =  -rho * gamma * summ * si2mg  * 1000.0

        return
    end subroutine

end module



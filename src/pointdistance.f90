!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate cumulative path distance of each point in a profile    
! from a commonpoint.  By default calculates distance from the     
! first point. Use -o<offset> to override. Uses Haversine formula  
! (more options to be added.)                                      
! Use: pointdistance [-f<filename>] [-o<offset>] [-u<unit>]        
!         OR  
!      pointdistance -h   for this message.                        
! f<filename>:      input filename.Else read from terminal(default)
! o<offset>  :      offset of first point.  Zero otherwise.        
! u<unit>    :      unit of distance. k=km(default), m=meter, n=NM 
!            :      <offset> should be in same unit if provided.   
! <table/file>:     lon lat somevalue (list based)                 
! output     :      lon lat somevalue distance                     
! mn(rewrote from Subeesh's)                                       
!
! LICENSE
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program pointdistance
        implicit none

        integer, parameter :: sp = kind(1e0)
        integer, parameter :: dp = kind(1.0d0)

        real(dp), parameter :: pi = 3.1415927

        integer :: i, j, k, err_flag
        real(dp) :: lon, lat, someval, offset, dist
        real(dp) :: plon, plat, tdist, shift_unit
        real(dp) :: cx, cy
        integer :: ios, uno, nargs
        character (len = 256) :: argsi
        character (len = 2  ) :: switchi
        character (len = 254) :: optioni
        character (len = 256) :: ifilename
        character             :: dist_unit
        character             :: dist_type

        logical :: if_flag = .FALSE.
        logical :: c_flag = .FALSE.

        nargs = command_argument_count()
        ios = 1
        uno = 5 
        offset = 0
        dist_unit = 'k'
        shift_unit = 1

        !READ ALL THE COMMANDLINE ARGUMENTS
        if( nargs .gt. 0 ) then

                do i = 1, nargs
                        call get_command_argument( i, argsi )
                        switchi = argsi(1:2)
                        optioni = argsi(3:)

                        if( switchi == "-f" ) then
                                ifilename = trim(optioni)
                                if_flag = .TRUE.
                        elseif( switchi == "-o" ) then
                                read( optioni, * ) offset
                        elseif( switchi == "-u" ) then
                                dist_unit = trim(optioni)
                        !THE FOLLOWING TWO ARE NOT IMPLEMENTED
                        elseif( switchi == "-m" ) then
                                dist_type = trim(optioni)
                        elseif( switchi == "-c" ) then
                                do j=1,len_trim(optioni)
                                        if( optioni(j:j) == ',' ) then
                                                optioni(j:j) = ' '
                                                c_flag = .TRUE.
                                        endif
                                enddo
                                if( c_flag .eqv. .TRUE. ) then
                                        read( optioni, *, IOSTAT=ios ) cx, cy
                                        if( ios .ne. 0 ) then
                                                c_flag = .FALSE.
                                        endif
                                endif
                                if( c_flag .eqv. .FALSE. ) then
                                        print *, "ERROR IN -c OPTION"
                                        call helpme()
                                endif
                        elseif( switchi == "-h" ) then
                              call helpme()
                              stop
                        endif
                enddo
        endif

        !TRY TO OPEN THE GIVEN FILENAME. IF NO FILENAME, OPEN STDIO
        if( if_flag .eqv. .TRUE. ) then
              uno = 16
              open( uno, FILE=ifilename, STATUS='OLD', IOSTAT=ios )
        else
              ios = 0
        endif

        if( ios /= 0 ) then
              print *, "FATAL ERROR: ERROR OPENING FILE"
              call helpme()
              stop
        endif

        !CHECK THE REQUESTED UNIT.  IF NOT, PROCEED WITH DEFAULT
        if( dist_unit == 'm' ) then
              shift_unit = 1000
        elseif( dist_unit == 'n' ) then
              shift_unit = 1.0/1.8
        endif

        !READ ONE LON, LAT PAIR - SPECIAL TREATMENT FOR FIRST
        !POINT. SKIP NON VALID LINES
        do
              read(uno, *, IOSTAT=ios) lon, lat, someval
              !CHECK FOR EOF
              if( ios .lt. 0 ) then
                      exit
              endif
              !CHECK FOR VALID LINES. SIKP IF INVALID
              if( ios .eq. 0 ) then
                      dist = 0 - offset  
                      tdist= dist ! total distance
                      plon = lon  ! previous lon
                      plat = lat  ! previous lat
                      print *, lon, lat, someval, tdist
                      exit
              endif
        enddo

        !NOW LOOP OVER TILL THE END 
        !SKIP NON-VALID LINES
        do
                read(uno, *, IOSTAT=ios) lon, lat, someval
                if( ios .lt. 0 ) then
                        exit
                endif
                if( ios .eq. 0 ) then
                        call haversine_distance&
                              ( lon, lat, plon, plat, dist )
                        dist = dist * shift_unit
                        plon = lon
                        plat = lat
                        tdist = tdist + dist
                        print *, lon, lat, someval, tdist
                endif
        enddo

        70 FORMAT (F15.10,1X,F14.10,1X,F7.2)

        CONTAINS

subroutine haversine_distance( lon1, lat1, lon2, lat2, dist )
! Implementation of Haversine Formula
! lon1, lat1 : previous loc
! lon2, lat2 : current loc
! dist       : distance(km) (return value)
! Haversine formula:-
! a = sin²(Δφ/2) + cos(φ1).cos(φ2).sin²(Δλ/2
! c = 2.atan2(√a, √(1−a))
! d = R.c
! where φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km)
! note that angles need to be in radians to pass to trig functions!

        real(dp), intent(in) :: lon1, lat1, lon2, lat2
        real(dp), intent(out) :: dist
        real(dp) :: delLonby2, delLatby2, lat1r, lat2r
        real(dp) :: deg2rad, a, a1, a2, c, e_rad

        deg2rad = pi / 180.0
        e_rad   = 6371.0 ! km 

        lat1r = lat1 * deg2rad
        lat2r = lat2 * deg2rad

        delLonby2 = ( lon2 - lon1 ) * deg2rad / 2.0 
        delLatby2 = ( lat2r - lat1r ) / 2.0

        a1 = sin( delLatby2 ) * sin( delLatby2 )
        a2 = ( cos(lat1r) * cos(lat2r) * sin(delLonby2) &
                * sin(delLonby2) )

        a  = a1 + a2

        c = 2 * atan2( sqrt(a), sqrt(1-a) )

        dist = e_rad * c 

endsubroutine haversine_distance

subroutine euclidean_distance( lat1, lon1, lat2, lon2, dist )

        real(dp), intent(in) :: lon1, lat1, lon2, lat2
        real(dp), intent(out) :: dist

        dist = sqrt( (lat2-lat1)**2 + (lon2-lon1)**2 )

endsubroutine euclidean_distance


subroutine helpme( )
     print *, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
     print *, "! Calculate cumulative path distance of each point in a profile    !"
     print *, "! from a commonpoint.  By default calculates distance from the     !"
     print *, "! first point. Use -o<offset> to override. Uses Haversine formula  !"
     print *, "! !(more options to be added.)                                     !"
     print *, "! -o<offset> to override. Uses Haversine formula(more options to be!"
     print *, "! added.                                                           !"
     print *, "! Use: pointdistance [-f<filename>] [-o<offset>] [-u<unit>]        !"
     print *, "! ! OR  pointdistance -h   for this message.                       !"
     print *, "! f<filename>:      input filename.Else read from terminal(default)!"
     print *, "! o<offset>  :      offset of first point.  Zero otherwise.        !"
     print *, "! u<unit>    :      unit of distance. k=km(default), m=meter, n=NM !"
     print *, "!            :      <offset> should be in same unit if provided.   !"
     print *, "! <table/file>:     lon lat somevalue (list based)                 !"
     print *, "! output     :      lon lat somevalue distance                     !"
     print *, "! mn(rewrote from Subeesh's)                                       !"
     print *, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
endsubroutine helpme

end program pointdistance



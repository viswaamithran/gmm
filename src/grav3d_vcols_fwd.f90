! program to compute gravity field of a body with given
! bathymetry surface as the upper bound, 'z_base' as the
! lower bound and infinite extension
! Calculates the gravity at the same points as bathymetry,
! along z = 0 plane
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
program grav3d_vcols_inv_density

    use IEEE_ARITHMETIC
    use omp_lib
    use dataFormats
    use nc_readwrite
    use gravity_simpleGeom
    use inversion_methods

    implicit none

    !INITIATION

    !Variables for handling command line arguments
    character (len = 256) :: argsi
    character (len = 2  ) :: switchi
    character (len = 254) :: optioni
    character (len = 254) :: bathy_filename
    character (len = 254) :: g_filename
    character (len = 254) :: op_file_name
    character (len = 254) :: lat_name1
    character (len = 254) :: lon_name1
    character (len = 254) :: z_name1

    !Single Precision is used for reading input values
    !Double Precision is used for calculations
    logical :: gferr, dnerr, bferr, oferr, lonerr
    logical :: laterr, zerr, berr, derr
    integer :: nargs, nthreads, i, j, ii, jj, k, l
    integer :: nlon, nlat, nx, ny, n1, n2
    real :: x0, x1, x2, y0, y1, y2, z0, z1, z2, z_base
    real :: dx, dy, dxby2, dyby2, rho
    real :: xlen, ylen, lon0, lat0, density
    real(dp) :: gg
    real, dimension(:), allocatable :: lon, lat, x, y
    real, dimension(:,:), allocatable :: topo
    real(dp), dimension(:,:), allocatable :: g_calc 

    !Flags which must be turned to false when corresponding
    !values are read from command line.
    data gferr, dnerr, bferr, oferr, lonerr, laterr, zerr, berr, derr/&
       .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE./

    !Read commandline arguments
    nargs = command_argument_count()

    !GET ARGUMENTS
    do i = 1, nargs
        !get i-th argument
        call get_command_argument( i, argsi )

        !parse it into switch and option
        switchi = argsi(1:2)
        optioni = argsi(3:)

        !lookup on option
        if ( switchi == "-b" ) then
            print *, "Bathymetry File: ", trim(optioni)
            bathy_filename = optioni
            bferr = .FALSE.

!        elseif( switchi == "-g" ) then
!            print *, "Gravity File: ", trim(optioni)
!            g_filename = optioni
!            gferr = .FALSE.

        elseif( switchi == "-d" ) then
            print *, "Density Contrast: ", trim(optioni)
            read(optioni, *) density
            dnerr = .FALSE.
            gferr = .FALSE.

        elseif( switchi == "-o" ) then
            print *, "output File: ", trim(optioni)
            op_file_name = optioni
            oferr = .FALSE.

        elseif( switchi == "-x" ) then
            print *, "x name: ", trim(optioni)
            lon_name1 = optioni
            lonerr = .FALSE.

        elseif( switchi == "-y" ) then
            print *, "y name: ", trim(optioni)
            lat_name1 = optioni
            laterr = .FALSE.

        elseif( switchi == "-z" ) then
            print *, "z name: ", trim(optioni)
            z_name1 = optioni
            zerr = .FALSE.

        elseif( switchi == "-B" ) then
            print *, "base: ", trim(optioni), " km"
            read(optioni, *) z_base
            berr = .FALSE.

        end if

    end do

    !Check whether we have all the necessary variables
    if ( gferr .or. bferr .or. oferr .or. lonerr .or. laterr &
        .or. zerr .or. berr ) then

        print *, "INSUFFICIENT ARGUMENTS. QUITTING"
        print *, gferr, bferr, oferr, lonerr, laterr, zerr, berr
        stop

    end if

    !Get lon, lat & bathymetry
    call read_2Dgrid( bathy_filename, lon_name1, lat_name1, &
        z_name1, lon, lat, topo )

    !Convert bathymetry from meters +ve upward to km +ve downward
    topo = -topo / 1000
    nlon = size(lon) 
    nlat = size(lat)

    !Shift origin to approximate centre of the grid.  Not necessary.
    !and convert to km.
    lon0= lon( int(nlon/2) )
    lat0= lat( int(nlat/2) )
    lon = ( lon-lon0 ) * 110
    lat = ( lat-lat0 ) * 110
    xlen= ( lon(nlon) - lon(1) )
    ylen= ( lat(nlat) - lat(1) )

    !Number of bathy points.  This will be the number of cells too.
    nx = nlon ! total number of cells along x
    ny = nlat ! total number of cells along y

    dx = lon(2) - lon(1)
    dy = lat(2) - lat(1)
    dxby2 = dx/2.0
    dyby2 = dy/2.0

    !print *, 'testeri3d3: bp-01: ', shape(topo), nlon, nlat

    !Allocation of dynamic memmory
    allocate(  x(nx+1) )
    allocate(  y(ny+1) )

    !Allocate & initialise dynamic memmory for 
    !calculated gravity matrix 
    allocate(  g_calc(nx, ny) )
    do i = 1, ny
    do j = 1, nx
        g_calc( nx,ny ) = 0.0
    enddo
    enddo

    !Co-ordinates of the cell boundaries
    !For x
    do i = 1, nx
        x(i) = lon(i) - dxby2
    enddo
    x(nx+1) = lon(nlon)+dxby2

    !For y
    do i = 1, ny
        y(i) = lat(i) - dyby2
    enddo
    y(ny+1) = lat(nlat)+dyby2

    !Boundary: Take first cell boundary outward to 10*dataLength
    x(1) = lon(1)-xlen*10 
    x(nx+1) = lon(nlon) + xlen*10 
    y(1) = lat(1)-ylen*10 
    y(ny+1) = lat(nlat) + ylen*10 

    !INVERSION

    !print *, 'testeri3d3: bp-02: ', shape(topo), nlon, nlat

    !Loop over the cells
    do j = 1, ny
    do i = 1, nx
        x1 = x(i)         !the four corners of each cell
        x2 = x(i+1)
        y1 = y(j)
        y2 = y(j+1)
        
        z1 = topo(i,j)
        z2 = z_base

        rho= density

        !Loop over all the obv points
        do jj = 1, ny
        do ii = 1, nx
            gg = 0
            x0 = lon(ii)
            y0 = lat(jj)
            z0 = 0.0
            call v_rect_prism( x0, y0, z0, x1, y1, z1, x2, y2, z2, rho, gg )
            g_calc(ii,jj) = g_calc(ii,jj) + gg
        enddo
        enddo
    enddo
    enddo

    !OUTPUT

    !Write output grid of n1xn2 values
    call ncwrite_2Dgrid_dp( op_file_name, lon_name1, lat_name1, &
        z_name1, lon, lat, g_calc )

    !EXIT

    deallocate(  x )
    deallocate(  y )
    deallocate( lon  )
    deallocate( lat  )
    deallocate( topo )
    deallocate(  g_calc  )

end program grav3d_vcols_inv_density




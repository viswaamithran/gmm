! fileio.f90           : contains necessary file ip/op routines
!                        to handle non-netcdf files
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
module fileio

    use dataFormats
    implicit none

contains

    subroutine read_chronfile( filename, age_s, age_e, sr_l, sr_r, &
            chron_n, pol, e_flag )
        !Subroutine to read chronfile
        !File Format  :
        !Input Params :
        !Output Params:
        !Version      :
        !Date         :
        integer :: i, pos, uno, ios, ierr4, nos_lines, e_flag
        character (len=256) :: l_buffer
        character (len=16 ) :: c_name

        logical, allocatable, intent(out) :: pol(:)
        character (len=*), intent(in) :: filename
        real, allocatable, intent(out) :: age_s(:), age_e(:), sr_l(:), sr_r(:)
        character(len=16), allocatable, intent(out) :: chron_n(:)

        e_flag = 0
        ierr4  = 0
        ios    = 0

        !ATTEMPT OPENING CHRON FILE
        uno = 10
        nos_lines = 0

        open( uno, FILE=filename, STATUS='OLD', IOSTAT=ios )

        if( ios/= 0 ) then
            print *, "FATAL ERROR: ERROR OPENING CHRON FILE"
            e_flag = 1
            return
        endif

        !READ THE NUMBER OF DATA LINES
        do
            read( uno, *, IOSTAT=ios ) l_buffer
            if( ios .lt. 0 ) exit
            if( l_buffer(1:1) .ne. '#' ) nos_lines = nos_lines + 1
        enddo
        rewind uno
        if( nos_lines .eq. 0 ) e_flag = 2

        !ALLOCATE MEMMORY
        if( e_flag .eq. 0 ) then
            allocate(   age_s(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(   age_e(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(    sr_l(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(    sr_r(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate( chron_n(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(     pol(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
        endif

        pol = .FALSE.

        !READ DATA
        if( e_flag .eq. 0 ) then
            i = 1 
            do
                read( uno, *, IOSTAT=ios ) sr_l(i), sr_r(i), age_s(i), age_e(i), chron_n(i)
                if( ios .lt. 0 ) exit

                !                if( (i .ne. 1) .and. (abs(age_s(i) - age_e(i-1)) > smallSigma) ) then
                !                    e_flag = 2
                !                    exit
                !                endif
                !DETERMINE THE POLARITY
                c_name = chron_n(i)
                pos = len(trim(c_name))
                if( c_name(pos:pos) .eq. 'n' ) pol(i) = .TRUE.

                !INCREMENT ITERATOR
                if( ios .eq. 0 ) i = i+1
            enddo
            if( i-1 .ne. nos_lines ) e_flag = 2 
        endif

        close( uno )

    end subroutine read_chronfile

    subroutine read_bathy_profile( filename, distance, depth, mag, e_flag )
        !Subroutine to read profile file
        !File Format  :
        !Input Params :
        !Output Params:
        !Version      :
        !Date         :

        character (len=*), intent(in)  :: filename
        real, allocatable, intent(out) :: distance(:), depth(:), mag(:)
        character ( len=256 ) :: l_buffer
        integer :: i, nos_lines, uno, ios, ierr4, e_flag

        e_flag = 0
        ierr4  = 0
        ios    = 0

        !ATTEMPT OPENING PROFILE FILE
        uno = 10
        nos_lines = 0

        open( uno, FILE=filename, STATUS='OLD', IOSTAT=ios )

        if( ios/= 0 ) then
            e_flag = 1
            return
        endif

        !READ THE NUMBER OF DATA LINES
        do
            read( uno, *, IOSTAT=ios ) l_buffer
            if( ios .lt. 0 ) exit
            if( l_buffer(1:1) .ne. '#' ) nos_lines = nos_lines + 1
        enddo
        rewind uno
        if( nos_lines .eq. 0 ) e_flag = 2

        !ALLOCATE MEMMORY
        if( e_flag .eq. 0 ) then
            allocate(   distance(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(      depth(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
            allocate(        mag(nos_lines), STAT=ierr4 )
            if( ierr4 .ne. 0 ) e_flag = 3
        endif

        !READ DATA
        if( e_flag .eq. 0 ) then
            i = 1 
            do
                read( uno, *, IOSTAT=ios ) distance(i), depth(i), mag(i)
                if( ios .ne. 0 ) exit
                if( ios .eq. 0 ) i = i+1
            enddo
            if( i-1 .ne. nos_lines ) e_flag = 2
        endif

        close( uno )

    endsubroutine read_bathy_profile

end module fileio

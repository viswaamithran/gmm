! nc_readwrite.f90     : contains function to read from and write to
!                        netcdf formatted files
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
module nc_readwrite

 use netcdf
 use dataFormats
 implicit none

 contains

 !!!SUBROUTINE TO READ 2D NETCDF GRID FILE!!!
 subroutine read_2Dgrid(FILE_NAME, LON_NAME, LAT_NAME, Z_NAME, lons, lats, zs)
 
 character (len= *), intent(in) :: FILE_NAME
 character (len = *), intent(in) :: LAT_NAME
 character (len = *), intent(in) :: LON_NAME
 character (len = *), intent(in) :: Z_NAME

 !Variables
 integer :: ncid, NLONS, NLATS, lon_dimid, lat_dimid, lon_varid, lat_varid, z_varid

 !Arrays
 real, allocatable, intent(out) :: lats(:), lons(:), zs(:,:)

 !Open the file
 !print *, 'Opening File: ', trim(file_name)
 call check( nf90_open(trim(FILE_NAME), nf90_nowrite, ncid) )

 !print *, ncid, trim(LON_NAME), trim(LAT_NAME)
 !Get dim-ids
 call check( nf90_inq_dimid(ncid, trim(LON_NAME), lon_dimid) )
 call check( nf90_inq_dimid(ncid, trim(LAT_NAME), lat_dimid) )

 !print *, LON_NAME, "(", lon_dimid, ")"
 !print *, LAT_NAME, "(", lat_dimid, ")"

 !Get dim lengths
 call check( nf90_inquire_dimension(ncid, lon_dimid, len=NLONS) )
 call check( nf90_inquire_dimension(ncid, lat_dimid, len=NLATS) )
 
 !print *, "NLOS=", NLONS, "NLATS=", NLATS

 !Allocate memmory
 allocate ( lats(NLATS) )
 allocate ( lons(NLONS) )
 allocate ( zs(NLONS, NLATS) )

 !Read dimensions
 call check( nf90_inq_varid(ncid, LAT_NAME, lat_varid) )
 call check( nf90_inq_varid(ncid, LON_NAME, lon_varid) )
 call check( nf90_inq_varid(ncid, Z_NAME, z_varid) )
 !print *, lat_varid, lon_varid

 !Read data
 call check( nf90_get_var(ncid, lon_varid, lons) )
 call check( nf90_get_var(ncid, lat_varid, lats) )
 call check( nf90_get_var(ncid, z_varid, zs ) )

 end subroutine read_2Dgrid

 !!!**************************************!!!

 !!!SUBROUTINE TO WRITE 2D NETCDF GRD FILE!!!
 subroutine ncwrite_2Dgrid(FILE_NAME, LON_NAME, LAT_NAME, Z_NAME, lons, lats, zs)

  character (len= *), intent(in) :: FILE_NAME
  character (len = *), intent(in) :: LAT_NAME
  character (len = *), intent(in) :: LON_NAME
  character (len = *), intent(in) :: Z_NAME
  character (len = *), parameter :: UNITS = "units"
  !Arrays
  real, allocatable, intent(in) :: lats(:), lons(:), zs(:,:)

  !Variables
  integer :: ncid, NLONS, NLATS, lon_dimid, lat_dimid, lon_varid, lat_varid, z_varid
  character (len = *), parameter :: lon_units= 'degree_east'
  character (len = *), parameter :: lat_units= 'degree_north'
  character (len = *), parameter :: z_units= 'mGal'
  integer :: dimids(2)

  !Here we go

  !Length of dims
  NLONS = size(lons)
  NLATS = size(lats)

  !Create netcdf file
  call check( nf90_create(FILE_NAME, NF90_CLOBBER, ncid) )

  !print *, "bc-1"
  !Define the dimensions
  call check( nf90_def_dim(ncid, trim(LON_NAME), NLONS, lon_dimid) )
  call check( nf90_def_dim(ncid, trim(LAT_NAME), NLATS, lat_dimid) )

  !print *, "bc-2"
  !Dimids
  dimids = (/ lon_dimid, lat_dimid /)

  !Define the variables
  call check( nf90_def_var( ncid, trim(LON_NAME), NF90_REAL, lon_dimid, lon_varid) )
  call check( nf90_def_var( ncid, trim(LAT_NAME), NF90_REAL, lat_dimid, lat_varid) )
  call check( nf90_def_var( ncid, trim(Z_NAME), NF90_REAL, dimids, z_varid) )

  !Assign units
  call check( nf90_put_att(ncid, lon_varid, UNITS, lon_units) )
  call check( nf90_put_att(ncid, lat_varid, UNITS, lat_units) )

  ! End define mode. This tells netCDF we are done defining metadata.
  call check( nf90_enddef(ncid) )

  !print *, "bc-3", z_varid
  !Add the data
  call check( nf90_put_var(ncid, lon_varid, lons) )
  call check( nf90_put_var(ncid, lat_varid, lats) )
  call check( nf90_put_var(ncid, z_varid, zs) )

  !print *, "bc-4"
  !Close the file
  call check( nf90_close(ncid) )

  !Done!
 end subroutine ncwrite_2Dgrid

 !!!*********************************************!!!
 subroutine ncwrite_2Dgrid_dp(FILE_NAME, LON_NAME, LAT_NAME, Z_NAME, lons, lats, zs)

  character (len= *), intent(in) :: FILE_NAME
  character (len = *), intent(in) :: LAT_NAME
  character (len = *), intent(in) :: LON_NAME
  character (len = *), intent(in) :: Z_NAME
  character (len = *), parameter :: UNITS = "units"
  !Arrays
  real, allocatable, intent(in) :: lats(:), lons(:)
  real(dp), allocatable, intent(in) ::  zs(:,:)

  !Variables
  integer :: ncid, NLONS, NLATS, lon_dimid, lat_dimid, lon_varid, lat_varid, z_varid
  character (len = *), parameter :: lon_units= 'degree_east'
  character (len = *), parameter :: lat_units= 'degree_north'
  character (len = *), parameter :: z_units= 'mGal'
  integer :: dimids(2)

  !Here we go

  !print *, 'ncwrite'
  !Length of dims
  NLONS = size(lons)
  NLATS = size(lats)

  !print *, 'bc-0'
  !Create netcdf file
  call check( nf90_create(FILE_NAME, NF90_CLOBBER, ncid) )

  !print *, "bc-1"
  !Define the dimensions
  call check( nf90_def_dim(ncid, trim(LON_NAME), NLONS, lon_dimid) )
  call check( nf90_def_dim(ncid, trim(LAT_NAME), NLATS, lat_dimid) )

  !print *, "bc-2"
  !Dimids
  dimids = (/ lon_dimid, lat_dimid /)

  !Define the variables
  call check( nf90_def_var( ncid, trim(LON_NAME), NF90_REAL, lon_dimid, lon_varid) )
  call check( nf90_def_var( ncid, trim(LAT_NAME), NF90_REAL, lat_dimid, lat_varid) )
  call check( nf90_def_var( ncid, trim(Z_NAME), NF90_REAL8, dimids, z_varid) )

  !Assign units
  call check( nf90_put_att(ncid, lon_varid, UNITS, lon_units) )
  call check( nf90_put_att(ncid, lat_varid, UNITS, lat_units) )

  !End define mode. This tells netCDF we are done defining metadata.
  call check( nf90_enddef(ncid) )

  !print *, "bc-3", z_varid
  !Add the data
  call check( nf90_put_var(ncid, lon_varid, lons) )
  call check( nf90_put_var(ncid, lat_varid, lats) )
  call check( nf90_put_var(ncid, z_varid, zs) )

  !print *, "bc-4"
  !Close the file
  call check( nf90_close(ncid) )

  !Done!
 end subroutine ncwrite_2Dgrid_dp
 !!!**********************************************!!!
 
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!SUBROUTINE FOR CHECKING STATUS OF NETCDF CALLS!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 subroutine check(status)
  integer, intent ( in) :: status
  
  if(status /= nf90_noerr) then 
   print *, trim(nf90_strerror(status))
   stop "Stopped"
  end if
 end subroutine check 

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!SUBROUTINE TO PRINT MATRIX TO TERMINAL        !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 subroutine print_matrix( desc, m, n, a, lda )
    use dataFormats

    character*(*)  ::  desc
    integer        ::  m, n, lda
    real(dp)       ::  a( m,n ) !lda, * )
!
    integer        ::  i, j
    character(LEN=20) :: mtrxline
!
    write( mtrxline, '(A,I4,A)')  '(', n,'(:,1x,E13.6))' 
!    print *, mtrxline
    write(*,*)
    write(*,*) desc
    do i = 1, m
       write(*,mtrxline) ( a( i, j ), j = 1, n )
    end do
!
9998 format( 11(:,1x,f6.2) )
    return
 endsubroutine print_matrix
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end module nc_readwrite


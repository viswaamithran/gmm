! program to invert a bouguer grid for density
! Reads bathymetry file and divides the earth below the bathymetry 
! surface down 'z_base' into a number of vertical columns, with each 
! bathymetry point being in the centre of the top surface of each 
! column. The program reads a bouguer anomaly file and tries to solve 
! the equation, A*m=g, where A is the matrix of geometrical coefficients
! and m is the matrix of denisty of each column, which  is unknown, 
! and g is the Bouguer gravity anomaly.  The equation is solved using 
! Moore-Pensrose pseudo inversion using SVD.  Lapack is used for SVD 
! calculation.
! Bathymetry is assumed to be -ve values with 0 being sea-surface and 
! in meters.  Gravity anomaly is assumed to be in mGal.  The result is 
! calculated in Kg/m^3.  Latitude and Longitdue are assumed in 
! decimal degrees.
! For any meaningful result, both the bathymetry and gravity should cover
! the same area and the number of gravity values should be at least 
! equal to the number of bathymetry values.
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
program grav3d_vcols_inv_density

    use IEEE_ARITHMETIC
    use omp_lib
    use dataFormats
    use nc_readwrite
    use gravity_simpleGeom
    use inversion_methods

    implicit none

    !INITIATION

    !Variables for handling command line arguments
    character (len = 256) :: argsi
    character (len = 2  ) :: switchi
    character (len = 254) :: optioni
    character (len = 254) :: bathy_filename
    character (len = 254) :: g_filename
    character (len = 254) :: op_file_name
    character (len = 254) :: lat_name1
    character (len = 254) :: lon_name1
    character (len = 254) :: z_name1

    !Single Precision is used for reading input values
    !Double Precision is used for calculations
    logical :: gferr, bferr, oferr, lonerr
    logical :: laterr, zerr, berr, derr
    integer :: nargs, nthreads, i, j, ii, jj, k, l
    integer :: nlon, nlat, nx, ny, n1, n2
    real :: x0, x1, x2, y0, y1, y2, z0, z1, z2, z_base
    real :: dx, dy, dxby2, dyby2, rho
    real :: xlen, ylen, lon0, lat0
    real(dp) :: gg, mean_g_obs
    real, dimension(:), allocatable :: lon, lat, x, y
    real, dimension(:), allocatable :: px, py
    real, dimension(:,:), allocatable :: g_obs, topo
    real(dp), dimension(:,:), allocatable :: A, rho_est

    !Flags which must be truned to false when corresponding
    !values are read from command line.
    data gferr, bferr, oferr, lonerr, laterr, zerr, berr, derr/&
       .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE., .TRUE./

    !Read commandline arguments
    nargs = command_argument_count()

    !GET ARGUMENTS
    do i = 1, nargs
        !get i-th argument
        call get_command_argument( i, argsi )

        !parse it into switch and option
        switchi = argsi(1:2)
        optioni = argsi(3:)

        !lookup on option
        if ( switchi == "-b" ) then
            print *, "Bathymetry File: ", trim(optioni)
            bathy_filename = optioni
            bferr = .FALSE.

        elseif( switchi == "-g" ) then
            print *, "Gravity File: ", trim(optioni)
            g_filename = optioni
            gferr = .FALSE.

        elseif( switchi == "-o" ) then
            print *, "output File: ", trim(optioni)
            op_file_name = optioni
            oferr = .FALSE.

        elseif( switchi == "-x" ) then
            print *, "x name: ", trim(optioni)
            lon_name1 = optioni
            lonerr = .FALSE.

        elseif( switchi == "-y" ) then
            print *, "y name: ", trim(optioni)
            lat_name1 = optioni
            laterr = .FALSE.

        elseif( switchi == "-z" ) then
            print *, "z name: ", trim(optioni)
            z_name1 = optioni
            zerr = .FALSE.

        elseif( switchi == "-B" ) then
            print *, "base: ", trim(optioni), " km"
            read(optioni, *) z_base
            berr = .FALSE.

        end if

    end do

    !Check whether we have all the necessary variables
    if ( gferr .or. bferr .or. oferr .or. lonerr .or. laterr &
        .or. zerr .or. berr ) then

        print *, "INSUFFICIENT ARGUMENTS. QUITTING"
        print *, gferr, bferr, oferr, lonerr, laterr, zerr, berr
        stop

    end if

    !Get lon, lat & bathymetry
    call read_2Dgrid( bathy_filename, lon_name1, lat_name1, &
        z_name1, lon, lat, topo )

    !Convert bathymetry from meters +ve upward to km +ve downward
    topo = -topo / 1000
    nlon = size(lon) 
    nlat = size(lat)

    !Shift origin to approximate centre of the grid.  Not necessary.
    !and convert to km.
    lon0= lon( int(nlon/2) )
    lat0= lat( int(nlat/2) )
    lon = ( lon-lon0 ) * 110
    lat = ( lat-lat0 ) * 110
    xlen= ( lon(nlon) - lon(1) )
    ylen= ( lat(nlat) - lat(1) )

    !Number of bathy points.  This will be the number of cells too.
    nx = nlon ! total number of cells along x
    ny = nlat ! total number of cells along y

    dx = lon(2) - lon(1)
    dy = lat(2) - lat(1)
    dxby2 = dx/2.0
    dyby2 = dy/2.0

!    print *, 'testeri3d3: bp-01: ', shape(topo), nlon, nlat

    !Allocation of dynamic memmory
    allocate(  x(nx+1) )
    allocate(  y(ny+1) )

    !Co-ordinates of the cell boundaries
    !For x
    do i = 1, nx
        x(i) = lon(i) - dxby2
    enddo
    x(nx+1) = lon(nlon)+dxby2

    !For y
    do i = 1, ny
        y(i) = lat(i) - dyby2
    enddo
    y(ny+1) = lat(nlat)+dyby2

    !Boundary: Take first cell boundary outward to 10*dataLength
    x(1) = lon(1)-xlen*10 
    x(nx+1) = lon(nlon) + xlen*10 
    y(1) = lat(1)-ylen*10 
    y(ny+1) = lat(nlat) + ylen*10 

    !Read bouguer anomaly grid
    !lon and lat variables are not overwritten- this will help if
    !the number of observation points(bouguer gravity) are different 
    !from the number of unknowns(cell densities)
    call read_2Dgrid( g_filename, lon_name1, lat_name1, &
        z_name1, px, py, g_obs )

    n1 = size(px) ! total number of obs points along x
    n2 = size(py) ! total number of obs points along y

    !Translate similar to lon lat and convert to km.
    px = ( px-lon0 ) * 110
    py = ( py-lat0 ) * 110

    !Allocate dynamic memmory for A, for which n1&n2 values are needed
    allocate(  A(n1*n2, nx*ny) )

    !INVERSION

    !Create coefficient matrix A of Am=g, using density value 1.0 Kg/m^3

    l = 1  ! k and l run through A(n1*n2, nx*ny).  Each outer loop moves
           ! from left to right through A and each inner loop runs from
           ! top to bottom, adding to the existing values.
    !Loop over the cells
    do j = 1, ny
    do i = 1, nx
        x1 = x(i)         !the four corners of each cell
        x2 = x(i+1)
        y1 = y(j)
        y2 = y(j+1)
        
        z1 = topo(i,j)
        z2 = z_base

        rho= 1.0

        k = 1
        
        !Loop over all the obv points
        do jj = 1, n2
        do ii = 1, n1
            gg = 0
            x0 = px(ii)
            y0 = py(jj)
            z0 = 0.0
            call v_rect_prism( x0, y0, z0, x1, y1, z1, x2, y2, z2, rho, gg )
            A(k,l) = gg
            k = k + 1
        enddo
        enddo
        l = l + 1
    enddo
    enddo

    !Convert gravity to vector and remove mean
    mean_g_obs = sum(g_obs) / size(g_obs)
    g_obs = g_obs - mean_g_obs

    !Psuedo-inverse - NOTE THAT ***A IS DESTROYED***
    rho_est = matmul( pinv_dp(A, 0.008), reshape(g_obs, [nx*ny,1]) )
    rho_est = reshape( rho_est, [nx,ny] )

    !OUTPUT

    !Write output grid of n1xn2 values
    call ncwrite_2Dgrid_dp( op_file_name, lon_name1, lat_name1, &
        z_name1, lon, lat, rho_est )

    !EXIT

    deallocate(  A )
    deallocate(  x )
    deallocate(  y )
    deallocate( px )
    deallocate( py )
    deallocate( lon  )
    deallocate( lat  )
    deallocate( topo )
    deallocate(  g_obs  )
    deallocate( rho_est )

end program grav3d_vcols_inv_density




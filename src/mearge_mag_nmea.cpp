/*
 * mearge_mag_nmea.cpp: combine R.V.Sagar Kanya raw magnetic data & GPS NMEA
 *                      and downsample to 1Hz.  Not tested for low freq. data
 *                      mag data format:<mag> <depth> <date> <time>
 *                      eg: $ 44230.614,1531  12/02/12 06:38:57.397
 *                      gps file should be in standard NMEA. $GPGL line is read
 *                      to extract lat, lon values.
 * USE                : mearge_mag_nmea -m<mag-filename> -g<gps-filename>
 * HISTORY            :
 *           start    : 2017.04.05 (V0.0)
 *  working jumble    : 2018.05.17 (V0.1)
 *            edit    : 2018.05.18
 * 			changed output format to Y/m/d H:M:S %15.10f %14.10f %8.2f  
 * 	      edit    : 2018.05.21
 * 			changed downsampling. Assumption of 10Hz input removed. 
 * 			Now averages for all data in the same integer second, 
 * 			irrespective of sampling freq.  Downsample to the same 1 Hz.
 *            edit    : 2018.08.03
 *                      rewrote this description
 *                      changed name from combineMag_n_gps to mearge_mag_nmea
 *                      included in gmm package
 *                      NOT TESTED FOR FOOLPROOFING
 *
 * LICENCE            :  GNU GPL v 3.0 or later
 * Copyright 2018 by Viswaamithran <viswaamithran@gmail.com>
 *
 * This file is part of the package gravity-magnetic-modeller(gmm)
 *
 * This program is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundatio,  version 3 of 
 * the License.
 * 
 * This source code is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <stdio.h>
#include <cstdio>
#include <string>
#include <argp.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_interp.h>

using namespace std;

/*
 * Read two files - gps and mag
 * make a table from each file - lon, lat, time and time, value, depth
 * interpolate lon, lat for each time in the second table
 * Output lon, lat, time, depth, value
 */

int main(int argc, char ** argv){

char c, s1[256], s2[256], s3[256], sdummy[256];
unsigned long int l_magdata=0, l_gpsdata=0;
int lon1, lat1;
long int l_1s_array=0;
float lon2, lat2;
char northing, easting;
float dt_magdata = 0.1; //sampling time for mag data
float m_sigma = 0.05; // precision required for float differences.

int i, j, r_val = 0, err_flag = 0;
double *gps_time=NULL;
int *mag_depth=NULL;
double *gps_lon=NULL, *gps_lat=NULL;
float *mag_val=NULL;
double * mag_time=NULL;
double * mag_lon=NULL, * mag_lat=NULL;
long int previous_time = 0;
long int* mag_time_1s=NULL;
double *mag_val_1s=NULL;

string gps_filename, mag_filename, line;

ifstream gps_file, mag_file;
unsigned int fsize_g, fsize_m, nos_lines_gps_file=0, nos_lines_mag_file=0;

struct tm res;
char buffer[26];
string time1, time2;

gsl_interp *workspace;
gsl_interp_accel *accel;

//Get the user supplied arguments
while( (c = getopt( argc, argv, "m:g:" )) != -1 ){
	switch (c){
		case 'm':
			mag_filename = optarg;
			//printf("m=%s\n", optarg);
			break;
		case 'g':
			gps_filename = optarg;
			//printf("g=%s\n", optarg);
			break;
		default:
			break;
	}
}

gps_file.open( gps_filename, std::ifstream::in );
mag_file.open( mag_filename, std::ifstream::in );

if( !gps_file | !mag_file )
	err_flag = 1;

if(err_flag==0){
	while( std::getline( gps_file, line ))
		++nos_lines_gps_file;
	gps_file.clear();
	gps_file.seekg(0, std::ios::beg);

	while( std::getline( mag_file, line ))
		++nos_lines_mag_file;
	mag_file.clear();
	mag_file.seekg(0, std::ios::beg);

//	std::cout<<"Nos. lines in gps file: "<<nos_lines_gps_file<<endl;
//	std::cout<<"Nos. lines in mag file: "<<nos_lines_mag_file<<endl;
}

//Create memmory locs for gps_lat, gps_lon, gps_time
if( err_flag == 0 ){
	gps_lon = new double[nos_lines_gps_file];
	gps_lat = new double[nos_lines_gps_file];
	gps_time= new double[nos_lines_gps_file];

	//Create memmory locs for mag_time, mag_depth, mag_value
	mag_time = new double[nos_lines_mag_file];
	mag_depth= new int[nos_lines_mag_file];
	mag_val  = new float[nos_lines_mag_file];

	if( !gps_lon | !gps_lat | !gps_time )
		err_flag = 3;
}
if(err_flag == 0){
	for(i=0; i<nos_lines_gps_file; i++){
		gps_lon[i] = 0;
		gps_lat[i] = 0;
		gps_time[i] = 0;
	}
	for (i = 0; i<nos_lines_mag_file; i++){
		mag_time[i] = 0;
		mag_depth[i] = 0;
		mag_val[i] = 0;
	}
}

//Read gps file and fill gps_lat, gps_lon, gps_time
if (err_flag == 0){
	j = -1;
	//sanitize the char strings
	while( getline( gps_file, line) ){
		for(i=1; i<256; i++){
			s1[i] = ' ';
			s2[i] = ' ';
			s3[i] = ' ';
		}
		//Divide into 3 the line starting with $GPGLL
		//$GPGLL,2303.97449,S,06832.38496,E,010422.00,A*16  12/02/12 06:38:57.892
		r_val = sscanf(line.c_str(), "$GPGLL,%s %s %s", s1, s2, s3);
		if(r_val==3){
			j++;
			//Extract lat and lon from first string
			r_val = sscanf(s1, "%2d%f,%c,%3d%f,%c,%s", &lat1, &lat2, &northing, &lon1, &lon2, &easting, &sdummy); 
			//Covert to decimal lat and lon with proper sign
			if(r_val==7){
				gps_lat[j] = lat1+lat2/60.0;
				if(northing=='S')
					gps_lat[j] = -gps_lat[j];
				gps_lon[j] = lon1+lon2/60.0;
				if(easting=='W')
					gps_lon[j] = -gps_lon[j];
			} // if - first string into 7 values - lat, lon

			//HANDLE TIME!!
			//Create struct tm object
			//Milli-seconds needs to be manhandled
			//Fist, the time
			time1 = string(s3);
			//time2 holds milli-seconds
			time2 = time1.substr( time1.find('.') );
			//Date-time
			time1 = string(s2)+" "+time1;
			strptime( time1.c_str(), "%D %T", &res);
			res.tm_isdst=0;
			strftime( buffer, 26, "%D %T", &res );
			gps_time[j] = mktime(&res)-timezone;
			if(gps_time[j]<=gps_time[j-1]) j--;	
		} // if - three strings are found
	} // while - each line
	l_gpsdata = j;
	gps_file.close();

	//Read mag file and fill mag_time, mag_depth, mag_value
	j=-1;
	while( getline( mag_file, line) ){
		for(i=1; i<256; i++){
			s1[i] = ' ';
			s2[i] = ' ';
			s3[i] = ' ';
		}
		//$ 44230.614,1531  12/02/12 06:38:57.397
		//read: <mag_val>, <mag_depth>, <date>, <time>
		//Reusing lon2, lon1 variables here - they ARE NOT LONGITUDES
		r_val = sscanf(line.c_str(), "$ %f,%d  %s %s", &lon2, &lon1, &s2, &s3);
		if (r_val == 4){
			j++;
			mag_val[j] = lon2;
			mag_depth[j] = lon1;
			time1 = string(s3);
			time2 = time1.substr( time1.find('.') );
			time1 = string(s2)+" "+time1;
			strptime( time1.c_str(), "%D %T", &res );
			res.tm_isdst = 0;
			mag_time[j] = mktime(&res) - timezone ;
			mag_time[j]+= std::stof(time2);
		}
	} // while - each line
	l_magdata = j;
	mag_file.close();
}

//Downsample Mag data to 1 Hz
if(err_flag==0){
	int i, j, k;
	int first_second = (int) mag_time[0];
	int last_second = (int) mag_time[0];
	int counter = 0;
	int rally_start = 0;
	double accu = 0;
	l_1s_array = l_magdata/10;
	l_1s_array = l_magdata%10? l_1s_array+1: l_1s_array;

	mag_val_1s = new double[l_1s_array];
	mag_time_1s = new long int[l_1s_array];
	for(i=0; i<l_1s_array; i++){
		mag_val_1s[i] = 0;
		mag_time_1s[i] = 0;
	}

	k = 0;

	for(i=0; i<l_magdata; i=j){
		for(j=i; j<l_magdata; j++){
			if(mag_time[j]>(1+last_second)){
				mag_time_1s[k] = last_second;
				last_second = (int) mag_time[j];
				mag_val_1s[k++] /= counter;
				counter = 0;
//				cout<< "\t" << mag_val_1s[k-1] << endl;
				break;
			}
			else{
				mag_val_1s[k] += mag_val[j];
				counter ++;
//				cout<< endl << mag_val[j];
			}
				
		}
	}
	l_1s_array = k-1;
}

if(err_flag==0){
	mag_lon = new double [l_1s_array];
	mag_lat = new double [l_1s_array];
	if(mag_lon==NULL | mag_lat==NULL)
		err_flag=5;
}
if(err_flag==0){
	for(i=0;i<l_1s_array; i++)
		mag_lon[i] = mag_lat[i] = 0;

	//Interpolate
	//GSL handles x and y as double- make time values smaller to avoid truncation probs!!!
//	for(i=0; i<l_1s_array; i++)
//		mag_time_1s[i] -= 1354400000;
//	for(i=0; i<l_gpsdata; i++)
//		gps_time[i] -= 1354400000;

	//Interpolate Latitude
	workspace = gsl_interp_alloc(gsl_interp_linear, l_gpsdata);

	accel = gsl_interp_accel_alloc();

	gsl_interp_init(workspace, (const double*)gps_time, (const double*)gps_lat, l_gpsdata);

	for(i=0;i<l_1s_array-1; i++){
		if((mag_time_1s[i] > gps_time[0]) && (mag_time_1s[i] < gps_time[l_gpsdata]) )
			mag_lat[i] = gsl_interp_eval(workspace, gps_time, gps_lat, 1.0*mag_time_1s[i], accel);
	}

	gsl_interp_free(workspace);

	//Interpolate Longitude
	workspace = gsl_interp_alloc(gsl_interp_linear, l_gpsdata);

	gsl_interp_init(workspace, gps_time, gps_lon, l_gpsdata);

	for(i=0;i<l_1s_array-1; i++)
		if((mag_time_1s[i] > gps_time[0]) && (mag_time_1s[i] < gps_time[l_gpsdata]) )
			mag_lon[i] = gsl_interp_eval(workspace, gps_time, gps_lon, mag_time_1s[i], accel);

	gsl_interp_free(workspace);

	for(i=1; i<256; i++)
		s1[i] = ' ';

	struct tm * timer;

	for(i=0;i<l_1s_array-1;i++){
		timer = localtime( (time_t*) &mag_time_1s[i] );
		strftime(s1, 256, "%Y/%m/%d %T", timer);
		cout<<s1<<".00 ";
		cout<<std::fixed<<std::setw(15)<<std::setprecision(10)<<mag_lon[i]<<" ";
		cout<<std::fixed<<std::setw(14)<<mag_lat[i]<<" ";
		cout<<std::fixed<<std::setprecision(2)<<mag_val_1s[i]<<endl;
	}

}

//Graceful Exit
if (err_flag)
	printf("Error: %d\n", err_flag);

return (err_flag);

}



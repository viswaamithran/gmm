! inversion_methods.f90: contains routines for  
!                        geophysical inversion
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!


module inversion_methods
    use dataFormats
    implicit none


    !integer, parameter :: dp = kind(1.0d0)

contains

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!SUBROUTINE TO CALCULATE INVERSE OF A SQUARE MATRIX            !!!
    !!!Taken from                                                    !!!
    !!!          http://fortranwiki.org/fortran/show/Matrix+inversion!!!
    !!!          Modified for real matrices                          !!!
    !!!          uses LU decomposition using LAPACK(sgetrf & sgetri) !!!
    !!!Inputs:                                                       !!!
    !!!      A: real, square matrix                                  !!!
    !!!Output:                                                       !!!
    !!!   Ainv: real, square matrix                                  !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    function inv_lu(A) result(Ainv)

        real, dimension(:,:), intent(in) :: A
        real, dimension(size(A,1),size(A,2)) :: Ainv

        real, dimension(size(A,1)) :: work  ! work array for LAPACK
        integer, dimension(size(A,1)) :: ipiv   ! pivot indices
        integer :: n, info

        ! External procedures defined in LAPACK
        external SGETRF
        external SGETRI

        ! Store A in Ainv to prevent it from being overwritten by LAPACK
        Ainv = A
        n = size(A,1)

        ! DGETRF computes an LU factorization of a general M-by-N matrix A
        ! using partial pivoting with row interchanges.
        call SGETRF(n, n, Ainv, n, ipiv, info)

        if (info /= 0) then
            stop 'Matrix is numerically singular!'
        end if

        ! DGETRI computes the inverse of a matrix using the LU factorization
        ! computed by DGETRF.
        call SGETRI(n, Ainv, n, ipiv, work, n, info)

        if (info /= 0) then
            stop 'Matrix inversion failed!'
        end if

    endfunction inv_lu

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!SUBROUTINE TO CALCULATE INVERSE OF A SQUARE MATRIX(DP)        !!!
    !!!Taken from                                                    !!!
    !!!          http://fortranwiki.org/fortran/show/Matrix+inversion!!!
    !!!          Modified for real matrices                          !!!
    !!!          uses LU decomposition using LAPACK(sgetrf & sgetri) !!!
    !!!Inputs:                                                       !!!
    !!!      A: real double pricision, square matrix                 !!!
    !!!Output:                                                       !!!
    !!!   Ainv: real double pricision, square matrix                 !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    function inv_lu_dp(A) result(Ainv)

        real(dp), dimension(:,:), intent(in) :: A
        real(dp), dimension(size(A,1),size(A,2)) :: Ainv

        real(dp), dimension(size(A,1)) :: work  ! work array for LAPACK
        integer, dimension(size(A,1)) :: ipiv   ! pivot indices
        integer :: n, info

        ! External procedures defined in LAPACK
        external DGETRF
        external DGETRI

        ! Store A in Ainv to prevent it from being overwritten by LAPACK
        Ainv = A
        n = size(A,1)

        ! DGETRF computes an LU factorization of a general M-by-N matrix A
        ! using partial pivoting with row interchanges.
        call DGETRF(n, n, Ainv, n, ipiv, info)

        if (info /= 0) then
            stop 'Matrix is numerically singular!'
        end if

        ! DGETRI computes the inverse of a matrix using the LU factorization
        ! computed by DGETRF.
        call DGETRI(n, Ainv, n, ipiv, work, n, info)

        if (info /= 0) then
            stop 'Matrix inversion failed!'
        end if

    endfunction inv_lu_dp

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
    !!!SUBROUTINE TO DO LINEAR LEAST SQUARES INVERSOIN OF DATA      !!!
    !!!Solves d = Gm                                                !!!
    !!!This is the single-pricision form                            !!!
    !!!Author: Viswaamithran                                        !!!
    !!!Inputs:                                                      !!!
    !!!      d: real array with shape n x 1                         !!!
    !!!      G: real array with shape n x m                         !!!
    !!!         where n is the number of equations(data points) and !!!
    !!!         m is the number of model parameters.                !!!
    !!!Output:                                                      !!!
    !!!      m: real array with shape m x 1 (model parameters)      !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    function linear_least_square(d, G) result(m)

        real, intent(in), dimension(:,:) :: d
        real, intent(in), dimension(:,:) :: G
        real, pointer, dimension(:,:):: m

        real, dimension(size(G,2), 1) :: Gtd
        real, dimension(size(G,2), size(G,2)) :: GtG

        !Remove any memmory attached with point m
        if ( associated(m) ) then
            deallocate(m)
        endif

        !Allot memmory to m
        nullify(m)
        allocate(m(size(G,2), 1))

        !print *, 'lls: bp-01', size(m), shape(G), size(d)
        !Calculate G' * d 
        Gtd = matmul( transpose(G), d )

        !print *, 'lls: bp-02'
        !Calculate G' * G
        GtG = matmul( transpose(G), G )

        !print *, 'lls: bp-03'

        m = reshape( matmul( inv_lu(GtG), Gtd ), [size(G,2), 1] )

        !Done
        !print *, 'lls: bp-04'
    endfunction linear_least_square

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
    !!!SUBROUTINE TO DO LINEAR LEAST SQUARES INVERSOIN OF DATA      !!!
    !!!Solves d = Gm                                                !!!
    !!!This is the double-pricision form                            !!!
    !!!Author: Viswaamithran                                        !!!
    !!!Inputs:                                                      !!!
    !!!      d: real array with shape n x 1                         !!!
    !!!      G: real array with shape n x m                         !!!
    !!!         where n is the number of equations(data points) and !!!
    !!!         m is the number of model parameters.                !!!
    !!!Output:                                                      !!!
    !!!      m: real array with shape m x 1 (model parameters)      !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    function linear_least_square_dp(d, G) result(m)

        real(dp), intent(in), dimension(:,:) :: d
        real(dp), intent(in), dimension(:,:) :: G
        real(dp), pointer, dimension(:,:):: m

        real(dp), dimension(size(G,2), 1) :: Gtd
        real(dp), dimension(size(G,2), size(G,2)) :: GtG

        !Remove any memmory attached with pointer m
        if ( associated(m) ) then
            deallocate(m)
        endif

        !Allot memory to m
        nullify(m)
        allocate(m(size(G,2), 1))

        !    print *, 'lls: bp-01', size(m)
        !Calculate G' * d 
        Gtd = matmul( transpose(G), d )

        !Calculate G' * G
        GtG = matmul( transpose(G), G )

        !    print *, 'lls: bp-03', shape(m), shape(Gtd), shape(inv_lu_dp(GtG))
        m = reshape( matmul( inv_lu_dp(GtG), Gtd ), [size(G,2),1] )

        !    print *, 'lls: bp-04'
        !Done
    endfunction linear_least_square_dp

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
    !!!SUBROUTINE TO DO FIND MOORE-PENROSE PSEUDO-INVERSE           !!!
    !!!USING LAPACK'S SVD                                           !!!
    !!!This is the double-pricision form                            !!!
    !!!Author: Viswaamithran                                        !!!
    !!!Inputs:                                                      !!!
    !!!        A: double array with size m x n                      !!!
    !!!      tou: (optional) real, the eig values below which are   !!!
    !!!           forced to zero(dafault: .001)                     !!!
    !!! lwmax_in: integer, size of working array(default: 1000)     !!!
    !!!Output:                                                      !!!
    !!!       ai: double array with size n x m A*ai=I or ai*A=I     !!!
    !!!           or both                                           !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    function pinv_dp(A, tou, lwmax_in) result(ai)

        real(dp), intent(in), dimension(:,:) :: A
        real(dp), pointer, dimension(:,:) :: ai, u, vt
        real(dp), pointer, dimension(:) :: s, work
        real(dp), dimension(1:2) :: work_querry
        real(dp) :: eig_min
        integer :: m, n, p, info, lwork
        integer :: lwmax, i
        integer, optional :: lwmax_in
        real, optional :: tou

        if( present(lwmax_in) ) then
            lwmax = lwmax_in
        else
            lwmax = 1000000
        endif
        if( present(tou) ) then
            eig_min = tou
        else
            eig_min = 1e-3
        endif

        m = size(A, 1)
        n = size(A, 2)
        p = min(m,n)

        allocate(  u(m,m) )
        allocate( vt(n,n) )
        allocate(  s(p)   )
        allocate( ai(n,m) )

        lwork = -1

        call dgesvd( 'all', 'all', m, n, A, m, s, u, m, vt, n, &
            work_querry, lwork, info )

        lwork = min( lwmax, int(work_querry(1)) )

        allocate( work(lwmax) )

        call dgesvd( 'A', 'A', m, n, A, m, s, u, m, vt, n, work, &
            lwork, info )

        u = transpose(u)
        vt = transpose(vt)

        if ( info==0 ) then
            do i = 1, p
                if( s(i) > eig_min ) then
                    s(i)=1.0/s(i)
                else
                    s(i)=0.0
                endif
            enddo
            do i = 1, p
                vt(:,i) = vt(:,i) * s(i) 
            enddo

            ai = matmul( vt(:,1:p), u(1:p,:) )

        else
            write(*,*) 'pinv_dp: error'
            stop
        endif

        deallocate( u )
        deallocate( vt )
        deallocate( s )
        deallocate( work )

    endfunction pinv_dp


endmodule inversion_methods

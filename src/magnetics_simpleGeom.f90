! magnetics_simpleGeom.f90: contains subroutines/functions to calculate
!                           magnetic field due bodies with simple
!                           geometry and known magnetisation params.
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
module magnetics_simpleGeom

    use dataFormats
    implicit none

contains

    subroutine dyke_2D( strength, x, w, z, Q, F )

        !!!***WARNING: NOT TESTED***!!!

        real(dp), intent(in) :: strength, x, w, z, Q
        real(dp), intent(out):: F
        real(dp) :: z2, r1, r2, rA2, rB2, thetaA_B
        real(dp) :: absx, term1, term2
        integer :: i, j, k

        z2  = z*z
        r1  = x+w
        r2  = x-w
        rA2 = z2 + r1*r1
        rB2 = z2 + r2*r2
        absx= abs( x )

        if( z .ne. 0.0 ) then
            thetaA_B = atan2( r1,z ) - atan2( r2,z )
        elseif( absx .eq. w ) then
            thetaA_B = piby2
        else
            thetaA_B = piby2*( 1 - (absx-w)/abs(absx-w) )
        endif

        if( rA2 .eq. 0.0 ) rA2 = smallEpsilon
        if( rB2 .eq. 0.0 ) rB2 = smallEpsilon

        term1 = cos(Q) * thetaA_B
        term2 = sin(Q) * 0.5 * log( rB2/rA2 )

        F = strength * ( term1 + term2 )

    endsubroutine dyke_2D


    subroutine v_dyke_2D( strength, x, xA, xB, z1, z2, Q, F )
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!subroutine to calculate total magnetic field anomaly    !!!
        !!!of a vertically dipping dyke                            !!!
        !!!Inputs:                                                 !!!
        !!!     strength : J*sin(pi/2), magnetisation              !!!
        !!!     x        : vector of observation locations(z=0)    !!!
        !!!     xA, xB   : x coords of top edges                   !!!
        !!!     z1, z2   : depth to top and bottom                 !!!
        !!!     Q        : difference between dip and declination  !!!
        !!!Outputs:                                                !!!
        !!!     F        : vector of total field anomaly calculated!!!
        !!!                at each x point                         !!!
        !!!Revisions:                                              !!!
        !!!     2018.06.25 : start and version 0.1                 !!!
        !!!version : 0.1                                           !!!
        !!!Reference:                                              !!!
        !!!        1. Murthy, I.V.R.(1992), Gravity and Magnetic   !!!
        !!!           Interpretation in Exploration Geophysics,    !!!
        !!!           Memoir 40, Geological Society of India       !!!
        !!!mn                                                      !!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        real(dp), intent(in) :: strength, z1, z2, Q
        real(dp), intent(in) :: xA, xB
        real(dp), intent(in) :: x(:)
        real(dp), intent(out):: F(:)
        real(dp), allocatable :: r1(:), r2(:), r3(:), r4(:)
        real(dp), allocatable :: theta1(:), theta2(:), theta3(:), theta4(:)
        real(dp), allocatable :: x1(:), x2(:)
        integer :: i, j, k

        x1 = x - xA 
        x2 = x - xB 

        theta1 = -atan2( x1,z1 )
        theta2 = -atan2( x2,z1 )
        theta3 = -atan2( x2,z2 )
        theta4 = -atan2( x1,z2 )

        r1 = x1*x1 + z1*z1
        r2 = x2*x2 + z1*z1
        r3 = x2*x2 + z2*z2
        r4 = x1*x1 + z2*z2

        where( r1 .lt. smallEpsilon ) r1 = smallEpsilon
        where( r2 .lt. smallEpsilon ) r2 = smallEpsilon

        F = cos(Q) * (theta1-theta2+theta3-theta4)
        F = F + sin(Q) * 0.5 * log( r2*r4/(r1*r3) )
        F = F * 2 * strength

    endsubroutine v_dyke_2D

    subroutine polygonal_cs_2D( M, D, Phi, strike, xi, zk, Px, dF )
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!subroutine to calculate total magnetic field anomaly    !!!
        !!!of a 2D body of polygonal cross-section                 !!!
        !!!Inputs:                                                 !!!
        !!!     M        : Strength of  magnetisation              !!!
        !!!     D        : Declination of magnetisation            !!!
        !!!     strike   : strike of the body                      !!!
        !!!     xi       : x coords of corners                     !!!
        !!!     zk       : z coords of corners                     !!!
        !!!     Px       : vector of observation locations(z=0)    !!!
        !!!Outputs:                                                !!!
        !!!     dF       : vector of total field anomaly calculated!!!
        !!!                at each x point                         !!!
        !!!Revisions:                                              !!!
        !!!     2018.06.26 : start and version 0.1                 !!!
        !!!version : 0.1                                           !!!
        !!!Reference:                                              !!!
        !!!        1. Murthy, I.V.R.(1992), Gravity and Magnetic   !!!
        !!!           Interpretation in Exploration Geophysics,    !!!
        !!!           Memoir 40, Geological Society of India       !!!
        !!!mn                                                      !!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        real(dp), intent(in) :: M, D, strike, Phi
        real, intent(in) :: xi(:), zk(:), Px(:)
        real(dp), allocatable, intent(out):: dF(:)

        integer :: i, j, nx, ncorners
        real :: cosphi, sinphi, phieff, mageff
        real, allocatable :: xip1(:), xk(:), xkp1(:), zkp1(:)
        real, allocatable :: thetaK(:), thetaKp1(:), rk2(:), rkp12(:)
        real, allocatable :: Sk(:), Ck(:), R2(:), R(:)
        real(dp), allocatable :: dFi(:)

        !print *, 'bp 01'

        nx       = size( Px )
        ncorners = size( xi )
        xip1     = cshift( xi, 1 )
        zkp1     = cshift( zk, 1 )
        cosphi   = cos(D)
        sinphi   = sin(D)

        !CALCULATE EFFECTIVE MAGNETISATION AND DIP
        mageff = 2 * M * sqrt( 1 - cos(Phi)*cos(Phi)*cos(strike)*cos(strike) )
!        mageff = M * sqrt( (cos(Phi)*cos(strike+pi/2-D))**2 + (sin(Phi))**2 )
        phieff = phi - atan( sin(strike-D) / tan(phi) )
        cosphi = cos( phieff )
        sinphi = sin( phieff )

        !print *, 'bp 02'

        allocate( xk      (ncorners) )
        allocate( xkp1    (ncorners) )
        allocate( rk2     (ncorners) )
        allocate( rkp12   (ncorners) )
        allocate( thetak  (ncorners) )
        allocate( thetakp1(ncorners) )
        allocate( Ck      (ncorners) )
        allocate( Sk      (ncorners) )
        allocate(  R      (ncorners) )
        allocate( R2      (ncorners) )
        allocate( dFi     (ncorners) )
        allocate( dF      (nx      ) )

        !print *, 'bp 03'

        !LOOP OVER EACH OBSERVATION POINT
        do i = 1, nx
            xk      = xi - Px(i)
            xkp1    = xip1 - Px(i)

            rk2     = xk**2 + zk**2
            rkp12   = xkp1**2 + zkp1**2
            thetak  = piby2 - atan2( xk, zk )
            thetakp1= cshift( thetak, 1 )
            !print *, 'bp 04: ', i

            where( (zk .eq. 0) .and. (xk .ne. 0) ) &
                    thetak = piby2 * ( 1-xk/abs(xk) )
            where( (zk .eq. 0) .and. (xk .eq. 0) ) &
                    thetak = piby2

            !print *, 'bp 05: ', i

            Ck      = (xkp1 - xk)
            Sk      = (zkp1 - zk)
            R2      = Ck**2 + Sk**2
            R       = sqrt( R2 )
            Ck      = Ck/R
            Sk      = Sk/R

            !print *, 'bp 06: ', i

            dFi     = Sk*( (Ck*cosphi+Sk*sinphi)*(thetakp1-thetak) + &
                (Ck*sinphi-Sk*cosphi)*0.5*log(rkp12/rk2) )

            !print *, 'bp 07: ', i

            dF(i)   = sum(dFi)
        enddo

        !print *, 'bp 04'
        dF = mageff * dF

        deallocate( xk      )
        deallocate( xkp1    )
        deallocate( rk2     )
        deallocate( rkp12   )
        deallocate( thetak  )
        deallocate( thetakp1)
        deallocate( Ck      )
        deallocate( Sk      )
        deallocate(  R      )
        deallocate( R2      )
        deallocate( dFi     )

    endsubroutine polygonal_cs_2D

    subroutine polygonal_cs_2D_ivrMurthy(aj, dm,  phi, strike, xx, zz, px, f_total )
        real, dimension(:), intent(in) :: xx, zz, px
        real(dp), intent(in) :: dm, aj, phi, strike
        real(dp), dimension(:), allocatable, intent(out) :: f_total

        integer :: i, n, k, k1 
        real(dp):: rad, str, dmr, sd, dm1, sn, tn, p, cosp, sinp, const
        real(dp):: dz, dx, r, s, c, t1, t2, rk1, rk, ak1, ak, dt
        real(dp), allocatable, dimension(:) :: x, z

        allocate( f_total( size(px) ) )
        do i = 1, size(px)
            f_total = 0.0
        enddo

        rad = 1 !0.0174532924 !ANGLES ARE ALREADY IN RADS
        str = rad * strike
        dmr = rad * dm
        sd  = cos(str) * cos(dmr)
        sd  = sd  * sd

        if( dm .eq. 0.0 ) then
            dm1 = 1.57079632
        else
            sn  = sin( str   )
            tn  = tan( dmr   )
            dm1 = atan( sn/tn )
        endif

        p = mod(phi - dm1, 2*pi)
        f_total = 0.0
        cosp = cos( p )
        sinp = sin( p )
        const = 2 * aj * sqrt( 1.0 - sd )

        do i = 1, size( px )
            x = xx - px(i)
            z = zz

            n = size( x )

            x(n+1) = x(1)
            z(n+1) = z(1)

            do k = 1, n
                k1  = k + 1
                dz  = z(k1) - z(k)

                if( dz .eq. 0.0 ) cycle

                dx  = x(k1) - x(k)
                r   = sqrt( dz*dz + dx*dx )

                if( r .eq. 0.0 ) cycle

                s   = dz/r
                c   = dx/r
                t1  = c * cosp + s * sinp
                t2  = c * sinp - s * cosp
                rk1 = x(k1)*x(k1) + z(k1)*z(k1)
                rk  = x(k)*x(k)   + z(k)*z(k)

                if( rk1 .eq. 0.0 ) then
                    ak1 = 0.0
                    rk1 = 1.0e-06
                elseif( z(k1) .eq. 0.0 ) then
                    ak1 = 1.570797632*x(k1) / abs( x(k1) )
                else
                    ak1 = atan( x(k1)/z(k1) )
                endif

                if( rk .eq. 0.0 ) then
                    ak  = 0.0
                    rk  = 1.0e-06
                elseif( z(k) .eq. 0.0 ) then
                    ak  = 1.57079632*x(k) / abs( x(k) )
                else
                    ak  = atan( x(k) / z(k) )
                endif

                t1 = t1 * ( ak-ak1 )
                t2 = 0.5 * t2 * log( rk1/rk )
                dt = const * s * (t1+t2)
                f_total(i) = f_total(i) + dt
            enddo

        enddo

    end subroutine polygonal_cs_2D_ivrMurthy

end module magnetics_simpleGeom

! dataFormats.f90      : contains necessary format descriptors
!                        and constant defenitions
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
module dataFormats
    integer, parameter :: sp = kind(1e0)
    integer, parameter :: dp = kind(1.0d0)
    real               :: smallSigma = 1e-3 !Floating-point round-off
    real               :: smallEpsilon = 1e-8 !Floating-point precision
    real(dp), parameter:: pi    = 3.141592653589793
    real(dp), parameter:: twopi = 6.283185307179586
    real(dp), parameter:: piby2 = 1.5707963267948966
end module

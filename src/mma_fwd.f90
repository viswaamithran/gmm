! Programme to calculate magnetic field produced by alternatively
! magnetised oceanic crust around MORs. It needs a chron file with
! format [left_spreading_rate right_spreading_rate starting_age 
! ending_age] and a profile file of format distance depth 
! magnetic_field. This uses formulae from I.V.R.Murthy's 1992 book
! titled 'Gravity and Magnetic Interpretation in Exploration 
! Geophysics'
! Tests: compared with Yatheesh & Bhattacharya's code magmod, and found
! that it is not exactly matching, though shapes are similar in general
! Thus is unfinished.
! version              : 0.1 
! Copyright 2017 by Viswaamithran <viswaamithran@gmail.com>
!
! This file is part of the package gravity-magnetic-modeller(gmm)
!
! This program is free software: you can redistribute 
! it and/or modify it under the terms of the GNU General Public 
! License as published by the Free Software Foundatio,  version 3 of 
! the License.
! 
! This source code is distributed in the hope that it will 
! be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
! of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
!
program mma_fwd
    !Usage: mma_fwd -a<age_file> -p<profile_file> -s<strike_angle(rad)>
    !       -D<geomag declination(rad)> -I<geomag inclination(rad)>
    !       -M<magnetisation(nT)> -i<inclination_of_M(rad)>
    !Chron File: <left_spreading_rate(mm/a)> <right_s_r(mm/a)> 
    !            <start_age(Ma)> <end_age(Ma)
    !Profile File: <distance(km)> <depth(m)> <dummy_val>
    !Returns: distance(m) magnetic_field(nT)

    use IEEE_ARITHMETIC
    use dataFormats
    use magnetics_simpleGeom
    use fileio

    implicit none

    character (len = 256) :: ch_filename, prof_filename
    integer :: i, j, nargs, nos_chrons, nos_obpoints
    integer :: e_flag, sr_flag, sp_flag, st_flag, start_x, stop_x
    integer :: nos_obpoints_l, nos_obpoints_r
    integer :: nos_corners_ibody
    real    :: dist_offset, tdistl, tdistr, dist0, dist1, thick
    logical :: chron_err, prof_err, str_err, m_err, phi_err
    logical :: gmphi_err, gmd_err

    real(dp) :: strength, x1, x2, z1, z2, Q
    real(dp) :: F, khi, M, D, Phi, strike, gm_d, gm_phi
    real(dp), dimension(101) :: x, dF
    real, allocatable :: xi(:), zi(:)
    real(dp), allocatable :: dF2(:), Ftot(:)
    real, allocatable :: age_1(:), age_2(:), sr_l(:), sr_r(:)
    logical, allocatable :: polarity(:)
    real, allocatable :: dist(:), depth(:), mag(:)
    real, allocatable :: cum_dist_l(:), cum_dist_r(:)
    character(len=16), allocatable :: ch_name(:)
    character (len = 256) :: argsi
    character (len = 2  ) :: switchi
    character (len = 254) :: optioni
    character (len = 254) :: bathy_filename
    character (len = 254) :: g_filename

    data chron_err, prof_err, str_err, m_err, phi_err/&
            .TRUE., .TRUE., .TRUE., .TRUE., .TRUE./
    data gmphi_err, gmd_err / .TRUE., .TRUE./

    !Read commandline arguments
    nargs = command_argument_count()

    do i = 1, nargs
        !get i-th argument
        call get_command_argument( i, argsi )

        !parse it into switch and option
        switchi = argsi(1:2)
        optioni = argsi(3:)

        !lookup on option
        if ( switchi == "-a" ) then
            !print *, "Age File: ", trim(optioni)
            ch_filename = trim(optioni)
            chron_err = .FALSE.
        elseif( switchi == "-p" ) then
            !print *, "Profile File: ", trim(optioni)
            prof_filename = trim(optioni)
            prof_err = .FALSE.
        elseif( switchi == "-s" ) then
            !print *, "Strike angle: ", trim(optioni)
            read(optioni, *) strike
            str_err = .FALSE.
        elseif( switchi == "-D" ) then
            !print *, "Geomagnetic Declination: ", trim(optioni)
            read(optioni, *) gm_d
            gmd_err = .FALSE.
        elseif( switchi == "-I" ) then
            !print *, "Geomagnetic Inclination: ", trim(optioni)
            read(optioni, *) gm_phi
            gmphi_err = .FALSE.
        elseif( switchi == "-M" ) then
            !print *, "Magnetisation: ", trim(optioni)
            read(optioni, *) M
            m_err = .FALSE.
        elseif( switchi == "-i" ) then
            !print *, "Magnetisation inclination: ", trim(optioni)
            read(optioni, *) Phi
            phi_err = .FALSE.
        endif
    enddo

    !CHECK WHETHER WE HAVE ALL THE NECESSARY PARAMETRES. IF NOT QUIT
    if( chron_err .or. prof_err .or. str_err .or. gmd_err .or. &
        gmphi_err .or. m_err .or. phi_err ) then

        print *, "INSUFFICIENT ARGUMENTS. QUITTING"
        print *, chron_err, prof_err, str_err, m_err, phi_err, &
            gmd_err, gmphi_err
        stop
    endif


!    ch_filename = "age_0_18m.data" !ck95.data"
!    prof_filename = "line03_nonan.xzm"

    !READ CHRON FILE
    call read_chronfile( ch_filename, age_1, age_2, sr_l, sr_r, &
        ch_name, polarity, sr_flag )
    if( sr_flag .ne. 0 ) call err_h( 100 + sr_flag )
    nos_chrons = size(age_1)

    !READ BATHYMETRY PROFILE
    call read_bathy_profile( prof_filename, dist, depth, mag, sr_flag )
    if( sr_flag .ne. 0 ) call err_h( 200 + sr_flag )
    nos_obpoints = size(dist)
    depth = -1 * depth
    dist  = dist * 1000

    !CREATE OUTPUT VECTOR FOR TOTAL ANOMALY FIELD
    allocate( Ftot(nos_obpoints) )
    Ftot = 0.0

    !CALCULATE OUTER BOUNDARIES OF EACH CHRON W.R.T. CENTRE
    allocate( cum_dist_l( nos_chrons ) )
    allocate( cum_dist_r( nos_chrons ) )
    tdistl = 0
    tdistr = 0
    do i = 1, nos_chrons
        tdistl        = ( age_2(i) - age_1(i) ) * sr_l(i)  + tdistl
        tdistr        = ( age_2(i) - age_1(i) ) * sr_r(i)  + tdistr
        cum_dist_l(i) = tdistl*1e+3
        cum_dist_r(i) = tdistr*1e+3
    enddo

    !FIND THE CORNERS OF EACH 'BODY' ON THE LEFT AND CALCULATE ANOMALY
    thick   = 500
    dist0   = 0
    sp_flag = 0

    !FIND THE START AND STOP POINTS IN THE DISTANCE VECTOR
    !CORRESPONDING TO THE CURRENT CHRON
    do i = 1, nos_chrons
        dist1 = -cum_dist_l(i)
        nos_corners_ibody = 0

        st_flag = 0
        stop_x = start_x
        do j = 1, nos_obpoints
            if( (st_flag .eq. 0) .and. (dist(j) .ge. dist1) .and. &
                (dist(j) .le. dist0) ) then
                start_x = j
                st_flag = 1
                if( sp_flag .eq. 1 ) exit
            elseif( (st_flag .eq. 1) .and. (sp_flag .eq. 0) .and. &
                (dist(j) .ge. dist0) ) then
                if( dist(j) .gt. dist0 ) then
                    stop_x = j-1
                else
                    stop_x = j
                endif
                sp_flag= 1
                exit
            endif
        enddo
        nos_corners_ibody = stop_x - start_x + 1
!        print *, i, start_x, stop_x, nos_corners_ibody
        if( (stop_x - start_x) .lt. 2 ) cycle

        allocate( xi( 2*nos_corners_ibody ) )
        allocate( zi( 2*nos_corners_ibody ) )
        xi(1:nos_corners_ibody)  = dist(start_x:stop_x)
        xi(nos_corners_ibody+1:) = dist(stop_x:start_x:-1)
        zi(1:nos_corners_ibody)  = depth(start_x:stop_x) 
        zi(nos_corners_ibody+1:) = depth(stop_x:start_x:-1) + thick

        if( polarity(i) .eqv. .FALSE. ) then
            Phi = gm_phi + pi !mod(gm_phi + pi, 2*pi)
            D   = gm_d + pi !mod(gm_d   + pi, 2*pi)
        else
            Phi = gm_phi
            D   = gm_d
        endif

!        call polygonal_cs_2D( M, D, Phi, strike, xi, zi, dist, dF2 )
        call polygonal_cs_2D_ivrMurthy( M, D, Phi, strike, xi, zi, dist, dF2 )

        Ftot = Ftot + dF2

        deallocate( dF2 )
        deallocate( xi  )
        deallocate( zi  )

        dist0 = dist1

    enddo

    !FIND THE CORNERS OF EACH 'BODY' ON THE RIGHT AND CALCULATE ANOMALY
    thick   = 500
    dist0   = 0
    st_flag = 0

    !FIND THE START AND STOP POINTS IN THE DISTANCE VECTOR
    !CORRESPONDING TO THE CURRENT CHRON
    do i = 1, nos_chrons
        dist1 = cum_dist_r(i)
        nos_corners_ibody = 0

        sp_flag = 0
        start_x = stop_x
        do j = 1, nos_obpoints
            if( (st_flag .eq. 1) .and. (sp_flag .eq. 0) .and. &
                (dist(j) .ge. dist1) ) then
                if( dist(j) .gt. dist1 ) then
                    stop_x = j-1
                else
                    stop_x = j
                endif
                sp_flag = 1
                exit
            elseif( (st_flag .eq. 0) .and. (dist(j) .ge. dist0) .and. &
                (dist(j) .le. dist1) ) then
                    start_x = j
                    st_flag = 1
                    sp_flag = 0
            endif
        enddo
        nos_corners_ibody = stop_x - start_x + 1

        allocate( xi( 2*nos_corners_ibody ) )
        allocate( zi( 2*nos_corners_ibody ) )
        xi(1:nos_corners_ibody)  = dist(start_x:stop_x)
        xi(nos_corners_ibody+1:) = dist(stop_x:start_x:-1)
        zi(1:nos_corners_ibody)  = depth(start_x:stop_x) 
        zi(nos_corners_ibody+1:) = depth(stop_x:start_x:-1) + thick

!        print *, i, start_x, stop_x, nos_corners_ibody

        if( polarity(i) .eqv. .FALSE. ) then
            Phi = gm_phi + pi !mod(gm_phi + pi, 2*pi)
            D   = gm_d + pi !mod(gm_d   + pi, 2*pi)
        else
            Phi = gm_phi
            D   = gm_d
        endif

!        call polygonal_cs_2D( M, D, Phi, strike, xi, zi, dist, dF2 )
        call polygonal_cs_2D_ivrMurthy( M, D, Phi, strike, xi, zi, dist, dF2 )

        Ftot = Ftot + dF2

        deallocate( dF2 )
        deallocate( xi  )
        deallocate( zi  )

        dist0 = dist1

    enddo
    do i = 1, nos_obpoints
        print *, dist(i), Ftot(i)
    enddo

endprogram mma_fwd

subroutine err_h( err )

    integer, intent(in) :: err
    integer :: err_n
    character ( len=256 ) :: taskname

    err_n = err

    if( err_n .le. 100 ) then
        taskname="mma_fwd:"
    elseif( (err_n .ge. 100) .and. (err_n .lt. 200) ) then
        taskname="READING CHRON FILE:"
        err_n = err_n - 100
    elseif( (err_n .ge. 200) .and. (err_n .lt. 300) ) then
        taskname="READING PROFILE FILE:"
        err_n = err_n - 200
    else
        taskname="mma_fwd:"
    endif

    if    ( err_n .eq. 1 ) then
        write(*,*) trim(taskname), ' ERROR IN OPENING FILE'
        stop
    elseif( err_n .eq. 2 ) then
        write(*,*) trim(taskname),  ' WRONG FORMAT IN INPUT FILE'
        stop
    elseif( err_n .eq. 3 ) then
        write(*,*) trim(taskname), ' ERROR IN MEMMORY ALLOCATION'
        stop
    else
        write(*,*) trim(taskname), ' ERROR(', err_n, '), BUT NO DESCRIPTION AVAIALBLE'
        stop
    endif

end subroutine err_h

